## DESCRIPTION

_t.rast.in.gpm_ is a tool to download, re-project and import data from the NASA/JAXA GPM satellite mission. 
(IMERG products - version: 03 or 04) 
The module will only download the area covered by your GRASS Region.

It only imports the level 3 IMERG v03 or v04 merged satellite-gauge precipitation (variables: precipitation for monthly data or precipitationCal for half-hourly and daily data).
The products that can be downloaded are:

* 3IMERGHHE: Half-hourly early
* 3IMERGHHL: Half-hourly late
* 3IMERGHH: Half-hourly final
* 3IMERGDE: Daily early
* 3IMERGDL: Daily late
* 3IMERGDF: Daily final
* 3IMERGM: Monthly

For the module to properly work, the following is needed:

* The [requests](http://docs.python-requests.org/en/master/) Python module
* The [NCO NetCDF toolbox](http://nco.sourceforge.net/), especially the _ncpdq_ executable
* Having a NASA Earthdata login
* [Authorize NASA GESDISC DATA ARCHIVE in Earthdata Login](https://disc.gsfc.nasa.gov/registration/authorizing-gesdisc-data-access-in-earthdata_login)
* [Fill-up the .netrc file in your home directory](https://disc.gsfc.nasa.gov/recipes/?q=recipes/How-to-Download-Data-Files-from-HTTP-Service-with-wget)

## AUTHORS

Laurent G. Courty, Instituto de Ingeniería UNAM, México
Roberta K. M. Kurek, Instituto de Ingeniería UNAM, México
